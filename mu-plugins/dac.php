<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/


/*
 * Load all mu-plugins
*/

/* Functions */
require_once(dirname(__FILE__) . '/dac/functions/sidebar-menus.php');
require_once(dirname(__FILE__) . '/dac/functions/menu-shortcode.php');
require_once(dirname(__FILE__) . '/dac/functions/acf_share_options.php');
require_once(dirname(__FILE__) . '/dac/functions/pagination.php');


/* Custom post types and taxonomies */
require_once(dirname(__FILE__) . '/dac/custom-post-types/dac-staff-board.php');
require_once(dirname(__FILE__) . '/dac/custom-post-types/injustice.php');
require_once(dirname(__FILE__) . '/dac/custom-post-types/itn.php');
require_once(dirname(__FILE__) . '/dac/custom-post-types/press.php');
require_once(dirname(__FILE__) . '/dac/custom-post-types/publication.php');
require_once(dirname(__FILE__) . '/dac/custom-post-types/resource.php');
