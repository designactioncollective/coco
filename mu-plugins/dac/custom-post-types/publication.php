<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_pub() {

	$labels = array(
		'name'                  => _x( 'Publications', 'Post Type General Name', 'coco' ),
		'singular_name'         => _x( 'Publication', 'Post Type Singular Name', 'coco' ),
		'menu_name'             => __( 'Publications', 'coco' ),
		'name_admin_bar'        => __( 'Publication', 'coco' ),
		'archives'              => __( 'Publication Archives', 'coco' ),
		'attributes'            => __( 'Publication Attributes', 'coco' ),
		'parent_item_colon'     => __( 'Parent Publication:', 'coco' ),
		'all_items'             => __( 'All Publications', 'coco' ),
		'add_new_item'          => __( 'Add New Publication', 'coco' ),
		'add_new'               => __( 'Add New Publication', 'coco' ),
		'new_item'              => __( 'New Publication', 'coco' ),
		'edit_item'             => __( 'Edit Publication', 'coco' ),
		'update_item'           => __( 'Update Publication', 'coco' ),
		'view_item'             => __( 'View Publication', 'coco' ),
		'view_items'            => __( 'View Publications', 'coco' ),
		'search_items'          => __( 'Search Publication', 'coco' ),
		'not_found'             => __( 'Not found', 'coco' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'coco' ),
		'featured_image'        => __( 'Featured Image', 'coco' ),
		'set_featured_image'    => __( 'Set featured image', 'coco' ),
		'remove_featured_image' => __( 'Remove featured image', 'coco' ),
		'use_featured_image'    => __( 'Use as featured image', 'coco' ),
		'insert_into_item'      => __( 'Insert into item', 'coco' ),
		'uploaded_to_this_item' => __( 'Uploaded to this publication', 'coco' ),
		'items_list'            => __( 'publications list', 'coco' ),
		'items_list_navigation' => __( 'Publications list navigation', 'coco' ),
		'filter_items_list'     => __( 'Filter publications list', 'coco' ),
	);
	$args = array(
		'label'                 => __( 'Publication', 'coco' ),
		'description'           => __( 'Post Type Description', 'coco' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'publication', $args );

}
add_action( 'init', 'custom_post_type_pub', 0 );
