<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_itn() {

	$labels = array(
		'name'                  => _x( 'In the News', 'Post Type General Name', 'coco' ),
		'singular_name'         => _x( 'In the News', 'Post Type Singular Name', 'coco' ),
		'menu_name'             => __( 'In the News', 'coco' ),
		'name_admin_bar'        => __( 'In the News', 'coco' ),
		'archives'              => __( 'In the News Archives', 'coco' ),
		'attributes'            => __( 'Item Attributes', 'coco' ),
		'parent_item_colon'     => __( 'Parent Item:', 'coco' ),
		'all_items'             => __( 'All In the News', 'coco' ),
		'add_new_item'          => __( 'Add New In the News', 'coco' ),
		'add_new'               => __( 'Add New', 'coco' ),
		'new_item'              => __( 'New Item', 'coco' ),
		'edit_item'             => __( 'Edit Item', 'coco' ),
		'update_item'           => __( 'Update Item', 'coco' ),
		'view_item'             => __( 'View Item', 'coco' ),
		'view_items'            => __( 'View Items', 'coco' ),
		'search_items'          => __( 'Search In the News', 'coco' ),
		'not_found'             => __( 'Not found', 'coco' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'coco' ),
		'featured_image'        => __( 'Featured Image', 'coco' ),
		'set_featured_image'    => __( 'Set featured image', 'coco' ),
		'remove_featured_image' => __( 'Remove featured image', 'coco' ),
		'use_featured_image'    => __( 'Use as featured image', 'coco' ),
		'insert_into_item'      => __( 'Insert into item', 'coco' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'coco' ),
		'items_list'            => __( 'In the News list', 'coco' ),
		'items_list_navigation' => __( 'In the News list navigation', 'coco' ),
		'filter_items_list'     => __( 'Filter In the News list', 'coco' ),
	);
	$rewrite = array(
		'slug'                  => 'in-the-news',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'In the News', 'coco' ),
		'description'           => __( 'Post Type Description', 'coco' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-clipboard',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'itn', $args );

}
add_action( 'init', 'custom_post_type_itn', 0 );
