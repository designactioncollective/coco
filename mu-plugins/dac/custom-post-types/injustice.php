<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_injust() {

	$labels = array(
		'name'                  => _x( 'Injustices', 'Post Type General Name', 'coco' ),
		'singular_name'         => _x( 'Injustice of the Week', 'Post Type Singular Name', 'coco' ),
		'menu_name'             => __( 'Injustices', 'coco' ),
		'name_admin_bar'        => __( 'Injustice of the Week', 'coco' ),
		'archives'              => __( 'Injustice of the Week Archives', 'coco' ),
		'attributes'            => __( 'Injustice of the Week Attributes', 'coco' ),
		'parent_item_colon'     => __( 'Parent Injustice:', 'coco' ),
		'all_items'             => __( 'All Injustices', 'coco' ),
		'add_new_item'          => __( 'Add New Injustice of the Week', 'coco' ),
		'add_new'               => __( 'Add New Injustice of the Week', 'coco' ),
		'new_item'              => __( 'New Injustice of the Week', 'coco' ),
		'edit_item'             => __( 'Edit Injustice', 'coco' ),
		'update_item'           => __( 'Update Injustice', 'coco' ),
		'view_item'             => __( 'View Injustice of the Week', 'coco' ),
		'view_items'            => __( 'View Injustices', 'coco' ),
		'search_items'          => __( 'Search Injustice', 'coco' ),
		'not_found'             => __( 'Not found', 'coco' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'coco' ),
		'featured_image'        => __( 'Featured Image', 'coco' ),
		'set_featured_image'    => __( 'Set featured image', 'coco' ),
		'remove_featured_image' => __( 'Remove featured image', 'coco' ),
		'use_featured_image'    => __( 'Use as featured image', 'coco' ),
		'insert_into_item'      => __( 'Insert into item', 'coco' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'coco' ),
		'items_list'            => __( 'Injustices list', 'coco' ),
		'items_list_navigation' => __( 'Injustices list navigation', 'coco' ),
		'filter_items_list'     => __( 'Filter Injustices list', 'coco' ),
	);
	$args = array(
		'label'                 => __( 'Injustice of the Week', 'coco' ),
		'description'           => __( 'Post Type Description', 'coco' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-visibility',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'injustice-of-the-week',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'injustice', $args );

}
add_action( 'init', 'custom_post_type_injust', 0 );
