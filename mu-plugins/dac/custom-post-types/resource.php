<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type_resource() {

	$labels = array(
		'name'                  => _x( 'Resources', 'Post Type General Name', 'coco' ),
		'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'coco' ),
		'menu_name'             => __( 'Resources', 'coco' ),
		'name_admin_bar'        => __( 'Resource', 'coco' ),
		'archives'              => __( 'Resource Archives', 'coco' ),
		'attributes'            => __( 'Resource Attributes', 'coco' ),
		'parent_item_colon'     => __( 'Parent Resource:', 'coco' ),
		'all_items'             => __( 'All Resources', 'coco' ),
		'add_new_item'          => __( 'Add New Resource', 'coco' ),
		'add_new'               => __( 'Add New Resource', 'coco' ),
		'new_item'              => __( 'New Resource', 'coco' ),
		'edit_item'             => __( 'Edit Resource', 'coco' ),
		'update_item'           => __( 'Update Resource', 'coco' ),
		'view_item'             => __( 'View Resource', 'coco' ),
		'view_items'            => __( 'View Resources', 'coco' ),
		'search_items'          => __( 'Search Resource', 'coco' ),
		'not_found'             => __( 'Not found', 'coco' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'coco' ),
		'featured_image'        => __( 'Featured Image', 'coco' ),
		'set_featured_image'    => __( 'Set featured image', 'coco' ),
		'remove_featured_image' => __( 'Remove featured image', 'coco' ),
		'use_featured_image'    => __( 'Use as featured image', 'coco' ),
		'insert_into_item'      => __( 'Insert into item', 'coco' ),
		'uploaded_to_this_item' => __( 'Uploaded to this resource', 'coco' ),
		'items_list'            => __( 'Resources list', 'coco' ),
		'items_list_navigation' => __( 'Resources list navigation', 'coco' ),
		'filter_items_list'     => __( 'Filter resources list', 'coco' ),
	);
	$args = array(
		'label'                 => __( 'Resource', 'coco' ),
		'description'           => __( 'Post Type Description', 'coco' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'custom-fields' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'resource', $args );

}
add_action( 'init', 'custom_post_type_resource', 0 );
