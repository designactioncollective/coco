<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// Register Custom Post Type
function custom_post_type_press() {

	$labels = array(
		'name'                  => _x( 'Press Releases', 'Post Type General Name', 'coco' ),
		'singular_name'         => _x( 'Press Release', 'Post Type Singular Name', 'coco' ),
		'menu_name'             => __( 'Press Releases', 'coco' ),
		'name_admin_bar'        => __( 'Press Release', 'coco' ),
		'archives'              => __( 'Item Archives', 'coco' ),
		'attributes'            => __( 'Event Attributes', 'coco' ),
		'parent_item_colon'     => __( 'Parent Press Release:', 'coco' ),
		'all_items'             => __( 'All Press Releases', 'coco' ),
		'add_new_item'          => __( 'Add New Press Release', 'coco' ),
		'add_new'               => __( 'Add New', 'coco' ),
		'new_item'              => __( 'New Press Release', 'coco' ),
		'edit_item'             => __( 'Edit Press Release', 'coco' ),
		'update_item'           => __( 'Update Press Release', 'coco' ),
		'view_item'             => __( 'View Press Release', 'coco' ),
		'view_items'            => __( 'View Press Releases', 'coco' ),
		'search_items'          => __( 'Search Press Release', 'coco' ),
		'not_found'             => __( 'Not found', 'coco' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'coco' ),
		'featured_image'        => __( 'Featured Image', 'coco' ),
		'set_featured_image'    => __( 'Set featured image', 'coco' ),
		'remove_featured_image' => __( 'Remove featured image', 'coco' ),
		'use_featured_image'    => __( 'Use as featured image', 'coco' ),
		'insert_into_item'      => __( 'Insert into Press Release', 'coco' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'coco' ),
		'items_list'            => __( 'Items list', 'coco' ),
		'items_list_navigation' => __( 'Items list navigation', 'coco' ),
		'filter_items_list'     => __( 'Filter items list', 'coco' ),
	);
	$args = array(
		'label'                 => __( 'Press Release', 'coco' ),
		'description'           => __( 'Post Type Description', 'coco' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'show_in_rest'					=> true,
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'press-release', $args );

}
add_action( 'init', 'custom_post_type_press', 0 );
