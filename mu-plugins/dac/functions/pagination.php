<?php
/*
Plugin Name: Design Action - Pagination
Description: Use the function pagination_simple() to add pagination
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
 * custom pagination with bootstrap .pagination class
 * source: http://www.ordinarycoder.com/paginate_links-class-ul-li-bootstrap/
 */
function bootstrap_pagination( $echo = true ) {
	global $wp_query;

	$big = 999999999; // need an unlikely integer

	$pages = paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'type'  => 'array',
			'prev_next'   => true,
			'prev_text'    => __('<i class="far fa-angle-left"></i><span class="sr-only">Next</span>'),
			'next_text'    => __('<i class="far fa-angle-right"></i><span class="sr-only">Previous</span>'),
		)
	);

	if( is_array( $pages ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');

		$pagination = '<nav aria-label="Archive navigation"><ul class="pagination pt-4">';

		foreach ( $pages as $page ) {
			$page = str_replace('page-numbers', 'page-link', $page);
			$active = (strpos($page, '>'.$paged.'<') !== false) ? ' active' : '';
			$disabled = (strpos($page, 'dots') !== false) ? ' disabled' : '';
			$pagination .= "<li class='page-item".$active.$disabled."'>$page</li>";
		}

		$pagination .= '</ul></nav>';

		if ( $echo ) {
			echo $pagination;
		} else {
			return $pagination;
		}
	}
}
