<footer class="content-info bg-light mt-5 pt-5 pb-5">
  <div class="container">
    <div class="row">
        <div class="col d-flex flex-column">
            <a class="btn btn-light" href="/wp-admin">Member Login</a>
            <a class="btn btn-primary" href="/get-updates">Get Email Updates</a>
        </div>
        <div class="col">
          <a class="twitter-timeline" data-height="225" href="https://twitter.com/cocoelections?ref_src=twsrc%5Etfw">Tweets by cocoelections</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div class="col">
          <a href='{{ home_url('/') }}' title='{{ get_bloginfo('name', 'display') }}' rel='home' class="pr-3">
            <img src='@asset('images/logo.png')' class='img-fluid' alt='{{get_bloginfo( 'name', 'display' )}}'>
          </a>
          @php dynamic_sidebar('sidebar-footer') @endphp
        </div>
    </div>
    <div class="row">
        {!! wp_nav_menu(['menu' => 6]) !!}
    </div>
  </div>
</footer>
