<div class="row">
  <?php if(has_post_thumbnail()) : ?>
    <div class="col embed-responsive">
      <a class="image-fit_wrap" href="<?php echo get_the_permalink(); ?>">
        <?php the_post_thumbnail('news'); ?>
      </a>
    </div>
  <?php endif; ?>
    <div class="col">
      <h2 class="mb-3"><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h2>
      @if(get_post_type() == 'itn')
      <?php the_time(); ?>
      @endif
    </div>

</div>
