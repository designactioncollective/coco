<?php $events = tribe_get_events(
              array(
                  'eventDisplay' => 'list',
                  'posts_per_page' => 2,
            )
      );
      if( !empty( $events) ) :
        foreach( $events as $event) {
          $return ='<div class="row event home-event">';
              //image
              $event_id = $event->ID;
              $return .= '<div class="col embed-responsive">';
              $return .= '<a class="image-fit_wrap" href="'.get_the_permalink($event_id).'" class="event-title">';
                $return .= '<img src="'.get_the_post_thumbnail_url($event_id, 'news').'">';
              $return .= '</a>';
              $return .= '</div>';
              $return .= '<div class="col">';

                $start_time = $event->EventStartDate;
                $return .= '<p class="date">'.date("F j, Y @ ga", strtotime($start_time)).'</p>';
                if(tribe_get_city($event_id) != ""){
                    $location = tribe_get_full_address($event_id);
                    //tribe_get_city($event_id).', 'tribe_get_state($event_id).'<br>'
                }
                //if( isset($location) ) $return .= '<p class="location">'.$location.'</p>';
                $return .= '<h2 class="mb-3"><a href="'.get_the_permalink($event_id).'" class="event-title">'.$event->post_title.'</a></h2>';
                if ( has_excerpt($event_id) )$return .= '<p class="entry-content hidden-xs">'.get_the_excerpt($event_id).'</p>';
              $return .= '</div>';
          $return .= '</div>';

          echo $return;
        }
      endif;

      ?>
