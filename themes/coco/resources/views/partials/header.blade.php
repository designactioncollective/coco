<header class="banner">
  <div class="container">
    <div class="row justify-content-between">
      <div class="col">
          <a href='{{ home_url('/') }}' title='{{ get_bloginfo('name', 'display') }}' rel='home' class="pr-3">
            <img src='@asset('images/logo.png')' class='img-fluid' alt='{{get_bloginfo( 'name', 'display' )}}'>
          </a>
          <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
      </div>
      <div class="col justify-content-end">
        <a class="btn btn-light" href="/wp-admin">Member Login</a>
        <a class="btn btn-primary" href="/get-updates">Get Email Updates</a>
      </div>
    </div>
    <div class="row justify-content-end">
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'slimmenu']) !!}
        @endif
      </nav>
    </div>
  </div>
</header>
