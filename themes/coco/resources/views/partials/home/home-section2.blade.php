<section class="home-3 overflow-hidden">
	<div class="container px-0 mt-5 mt-md-0">
		<div class="row">
			<div class="col">
					<div class="row">
						<h2 class="display-4 text-uppercase text-center mb-3"><a class="text-body" href="<?php echo site_url(); ?>/injustice-of-the-week">Injustice of the Week</a></h2>
					</div>
					<?php
					$args = array(
						'post_type' => 'injustice',
						'posts_per_page' => 1,
						//'order_by' => 'ASC',
						//'meta_query'  => array(
						//	array(
						//		'key' => 'featured',
						//		'compare' => '=',
						//		'value' => 1
						//	)
					//	)
					);
					$the_query = new WP_Query( $args ); ?>
					<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						@include('partials.content-home-1')
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
					<?php endif; ?>

		</div>
		<div class="col">
			<div class="row">
				<h2 class="display-4 text-uppercase text-center mb-3"><a class="text-body" href="<?php echo site_url(); ?>/in-the-news">In the News</a></h2>
			</div>
			<?php
			$args = array(
				'post_type' => 'itn',
				'posts_per_page' => 2,
				//'order_by' => 'ASC',
			//	'meta_query'  => array(
				//	array(
				//		'key' => 'featured',
				//		'compare' => '=',
				//		'value' => 1
				//	)
			//	)
			);
			$the_query = new WP_Query( $args ); ?>
			<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="">
							<h2 class="mb-3"><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php else : ?>
			<?php endif; ?>
		</div>
  	</div>
	</div>
</section>
