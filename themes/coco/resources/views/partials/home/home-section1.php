<?php
$image = get_field('hero_image');
?>
<div class="py-4 bg-primary text-center text-white" style="filter: none;">
	<div class="container">
		<h2 class="display-3 text-uppercase">
			<?php
			if(get_field('hero_headline')):
				the_field('hero_headline');
			endif;
			?>
		</h2>
		<p class="h4 neusaNextPro-light">
			<?php
			if(get_field('hero_text')):
				echo get_field('hero_text');
			endif;
			?>
		</p>
	</div>
</div>

<section class="home-1">
	<div class="overflow-hidden">
		<?php
		if( have_rows('hero_links') ):
			echo '<ul class="d-none hero-lnks position-absolute d-sm-flex list-unstyled w-100" style="z-index: 2;">';
			while ( have_rows('hero_links') ) : the_row();
			echo '<li class="border-white border-top-0 border-bottom-0 border-left-0 border-0_last-child flex-fill text-center btn rounded-0 btn-secondary py-lg-4 py-3 px-lg-3 px-1 h5 text-uppercase" style="opacity: 0.8;"><a class="text-gray-800 h6 h5-lg" href="'. get_sub_field('hero_link') .'">'. get_sub_field('hero_link_text') .'</a></li>';
		endwhile;
		echo '</ul>';
		else :
		endif;
		?>
	</div>

</section>

<div class="embed-responsive embed-responsive-24by49">
	<a href="<?php if(get_field('hero_image_link')): the_field('hero_image_link'); endif; ?>" class="image-fit_wrap">
		<img src="<?php echo $image['url']; ?>" />
	</a>
</div>
