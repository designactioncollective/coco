<section class="home-3 overflow-hidden">
	<div class="container px-0 mt-5 mt-md-0">
		<div class="row">
			<div class="col">
					<div class="row">
						<h2 class="display-4 text-uppercase text-center mb-3"><a class="text-body" href="<?php echo site_url(); ?>/events">Upcoming Events</a></h2>
					</div>

					@include('partials/inserts/event_widget')

		</div>
		<div class="col">
			<div class="row">
				<h2 class="display-4 text-uppercase text-center mb-3"><a class="text-body" href="<?php echo site_url(); ?>/resource">Featured Resources</a></h2>
			</div>
			<?php
			$args = array(
				'post_type' => 'resource',
				'posts_per_page' => 3,
				//'order_by' => 'ASC',
			//	'meta_query'  => array(
				//	array(
				//		'key' => 'featured',
				//		'compare' => '=',
				//		'value' => 1
				//	)
			//	)
			);
			$the_query = new WP_Query( $args ); ?>
			<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="">
							<h2 class="mb-3"><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php else : ?>
			<?php endif; ?>
		</div>
  	</div>
	</div>
</section>
